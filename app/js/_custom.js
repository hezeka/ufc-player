document.addEventListener("DOMContentLoaded", function() {
	var player1 = document.getElementById('player')


	function setPlayerSize (element) {
		var width = element.offsetWidth
		var height = (width / 16) * 9 
		element.style.height = height + 'px' 
		console.log(height, width)
	}

	setPlayerSize(player1)

	setInterval(function(){
		setPlayerSize(player1)
	},1000);

	window.addEventListener('resize', function() {
		setPlayerSize(player1)
	}, true)

});
